# TPheritage

## About : university.py :

This script, using OOP, creates the necesary structure of a fictional university that allows to enter students, teachers, grades, and to return the scores og the different lessons, as long with the mean and the missing scores.

## Features :

**Main Feature** :
This script, using OOP, creates the necesary structure of a fictional university

**Optional Features** :



## Installation :

To install this script use the following commands :

```bash
mkdir ~/Desktop/MORALES/
cd ~/Desktop/MORALES/
git clone https://gitlab.com/gmoralest/tpheritage
```
==> You're now good to go!

## Usage :

For information on using the script you can type the following commands :

```bash
cd ~/Desktop/MORALES/
python3 university.py
```

## Authors :

**Gabriel Morales**
