#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Creation of a university organization structure, using OOP,
that allow to enter students, grades, teachers, and to obtain
the grades, the mean scores and the missing scores
"""

__author__ = ' Gabriel Morales'



class Person:
    """ Person Class  """
    def __init__(self, name, last_name, role, phone, e_mail):
        self.name = name
        self.last_name = last_name
        self.role = role
        self.phone = phone
        self.e_mail = e_mail

    def get_name(self):
        """ Return name of the person """
        return self.name

    def get_last_name(self):
        """ Return last name of the person"""
        return self.last_name

    def get_phone(self):
        """ Return Phone number of the person"""
        return self.phone

    def get_e_mail(self):
        """ return E-mail of the person"""
        return self.e_mail

    def get_role(self):
        """ Return role of the person"""
        return self.role

    def __str__(self):
        """ Return a string with all the information in the attributes"""
        string = self.role + " name : " + str(self.name) + " " + str(
            self.last_name) + "\n" + "Telephone : " + str(
                self.phone) + "\n" + "E-mail : " + str(self.e_mail) + "\n"
        return string


class Student(Person):
    """  Student Class"""
    def __init__(self, name, last_name, phone, e_mail, entry_year, grades={}):
        super().__init__(name, last_name, "student", phone, e_mail)
        self.entry_year = entry_year
        self.grades = grades

    def add_grade(self, lesson, score):
        """ Allows to insert a new grade score to the student"""
        self.grades.update({lesson: score})

    def get_grades(self):
        """ Return all the grades of the student"""
        return self.grades

    def missing_grades(self):
        """ return all the grades that """
        for i in self.grades:
            if self.grades[i] is None:
                print(i)

    def get_mean(self):
        """ Return the mean score of the grades"""
        mean_score = 0
        for i in self.grades:
            if self.grades[i] is None:
                mean_score += 0        
            else:
                mean_score += self.grades[i]
        return mean_score / len(self.grades)


class Professor(Person):
    """Professor class"""


    def __init__(self, name, last_name, phone, e_mail, start_date, salary, departament=0):
        super().__init__(name, last_name, "professor", phone, e_mail)
        self.start_date = start_date
        self.salary = salary
        self.departament = departament

    def get_start_date(self):
        """ Return the starting working date of the professor at the university"""
        return self.start_date


STUDENT2 = Person("Gabriel", "Morales", " Student", "+3333333333", "fsaas@gmail.com")
STUDENT1 = Student("juan", "perez", "+123123123", "asdsad@gmail.com", 2021)
PROFESSOR1 = Professor("ana", "lala", "+87653421",
                       "ana@gmail.com", "21/08/99", 1000)
PROFESSOR2 = Professor("aquiles", "traigo", "+765234244",
                       "aquiles@gmail.com", "4/3/2", 5000)

STUDENT1.add_grade("math", 13.0)
STUDENT1.add_grade("science", None)
STUDENT1.add_grade("history", None)
STUDENT1.add_grade("english", 4.0)

print(STUDENT1)
print(STUDENT1.get_grades())
STUDENT1.missing_grades()
print(STUDENT1.get_mean())
